//
// B.RF Group
// dmalex
//
import { useState, useEffect, useRef } from "react"
import { useSelector, useDispatch } from "react-redux"
import { addTodo, removeTodo } from "./features/todoSlice"
import TodoItem from "./components/TodoItem"
import "./App.css"

function App() {
  const [input, setInput] = useState("")
  const textInput = useRef(null) // реф для доступа

  const todos = useSelector((state) => state.todo.todos)
  const dispatch = useDispatch()

  useEffect(() => {
    textInput.current.focus()
  }, [])

  const handleAddTodo = (e) => {
    e.preventDefault()
    if (input !== "") {
      dispatch(addTodo(input))
      setInput("")
    }
    textInput.current.focus()
  }

  const handleTodoDone = (id) => {
    dispatch(removeTodo(id))
    setInput("")
    textInput.current.focus()
  }

  return (
    <div className="App">
      <h1 className="App-title">Todo List</h1>
      <form className="App-form" onSubmit={handleAddTodo}>
        <input type="text" value={input} onInput={(e) => setInput(e.target.value)} ref={textInput}/>
        <button type="submit">Add</button>
      </form>
      <div className="Todos">
        {todos.map((todo) => (
            <TodoItem
              key={todo.id}
              text={todo.text}
              id={todo.id}
              onCheck={handleTodoDone}
            />
        ))}
      </div>
    </div>
  )
}

export default App
